# Reversing

[Ida 7.0 Freeware](https://www.hex-rays.com/products/ida/support/download_freeware.shtml)

## Brother PTouch

- http://www.odorik.cz/w/ptouch
- http://apz.fi/blabel/
- https://askubuntu.com/questions/638199/p-touch-editor-for-ubuntu-14-04
- http://www.openprinting.org/driver/ptouch/
- https://github.com/cbdevnet/pt1230
- https://github.com/dradermacher/ptouch-print
- https://github.com/abelits/ptouch-770
- https://github.com/talpadk/ptouch-php

## Android Apps

- https://www.evilsocket.net/2017/04/27/Android-Applications-Reversing-101/
- https://www.rsaconference.com/writable/presentations/file_upload/stu-w02b-beginners-guide-to-reverse-engineering-android-apps.pdf
- https://ibotpeaches.github.io/Apktool/
- https://medium.com/@thomas_shone/reverse-engineering-apis-from-android-apps-part-1-ea3d07b2a6c

## Sony Ericsson

- [GitLab repository](https://gitlab.com/TomasHubelbauer/bloggo-se)
- [Bloggo post](http://hubelbauer.net/post/bloggo-se)
